#Mark Boady

#Make all the plots based on JSON list

#Libraries
import sys
import json
import change
import os
import os.path

#Make all the images
def main(argv):
    if len(argv)!=4:
        print("Usage: python all_plots.py [json list] [folder] [csv]")
        return
    filename = argv[1]
    folder = argv[2]
    csvFile = argv[3]
    #Open the JSON
    f = open(filename,"r")
    data = f.read()
    f.close()
    courseList = json.loads(data)
    #Loop over all the courses
    for courseID in courseList:
        tName = "cs"+courseID+"_rating.jpg"
        target = os.path.join(folder,tName)
        change.main(["all_plots.py",courseID, csvFile,target])

if __name__=="__main__":
    main(sys.argv)
    
