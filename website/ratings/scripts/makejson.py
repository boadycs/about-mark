#Mark Boady
#Turn CSV file into nice JSON

#Assumes the first line of the file is the column names

#Libraries
import sys
import json
import csv

#Remove Null Strings
def cleanString(row):
    pos = row.index("")
    return row[:pos]

#Parse the File into a cool data structure
def parse(filename):
    f=open(filename,"r")
    #Convert to CSV
    reader = csv.reader(f)
    #Parse the title column
    titles = cleanString(next(reader))
    #Parse the rows
    data=[]
    for line in reader:
        data.append(cleanString(line))
    #Close up Shop
    f.close()
    #Make the Final DB
    DB={}
    DB["titles"]=titles
    DB["rowCount"]=len(data)
    DB["data"]=data
    return DB

#Write output
def writeFile(DB, target):
    f = open(target,"w")
    f.write(json.dumps(DB))
    f.close()

#Deal with command line arguments
def main(argv):
    if len(argv)!=3:
        print("Usage: python makejson.py [csv file] [json file]")
        return
    inputFile = argv[1]
    outputFile = argv[2]
    DB = parse(inputFile)
    writeFile(DB,outputFile)

if __name__=="__main__":
    main(sys.argv)
