#Mark Boady
#Generate Plots Based on Course Ratings
import csv
import matplotlib.pyplot as plt
import sys
import os

#We need to parse out the file into a database
#dictionary keys (year, term, course)
#dictionary values (course Rating, count, instructor rating)
def parseDatabase(filename):
    #Make an empty database
    database={}
    #Open the file for reading
    #We assume you aren't dumn and do no error checking
    f=open(filename,"r")
    reader = csv.reader(f)
    #Loop over the file
    for line in reader:
        #Skip the header column
        if line[1]=="Section":
            continue
        #Parse the Line
        course = int(line[0])
        year = int(line[2])
        term = int(line[3])
        response = int(line[4])
        crRating = float(line[6])
        inRating = float(line[7])
        #If no course rating, that means
        #not enough people submitted
        if crRating < 1:
            continue
        #Generate a tuple for the dictionary Key
        key = (year,term,course)
        #If already in DB, update (multiple sections)
        tuple = [crRating,response,inRating]
        if key in database:
            #database[key][0]+=crRating
            #database[key][1]+=response
            #database[key][2]+=inRating
            database[key].append(tuple)
        else:
            #Else add new record
            #database[key] = [crRating,response,inRating]
            database[key] = [tuple]
        #Collected data
        specialKey = (year,term,0)
        if specialKey in database:
            database[specialKey].append(tuple);
            #database[specialKey][0]+=crRating
            #database[specialKey][1]+=1
            #database[specialKey][2]+=inRating
        else:
            #Else add new record
            #database[specialKey] = [crRating,1,inRating]
            database[specialKey] = [tuple]
            
    #Return the Database
    return database

#Get the Courses from the DB
def getCourses(DB):
    keys = list(DB.keys())
    #Use a set since we need to duplicates
    courseList = set([])
    for k in keys:
        courseList.add(k[2])
    #Make into a sorted list
    courseList = list(courseList)
    courseList.sort()
    return courseList
    
#Get the Years from the DB
def getYears(DB):
    keys = list(DB.keys())
    #Use a set since we need to duplicates
    yearList = set([])
    for k in keys:
        yearList.add(k[0])
    #Make into a sorted list
    yearList = list(yearList)
    yearList.sort()
    return yearList

#Determine the name of the term
def termName(year,term):
    termNames={1:"Fall",2:"Winter",3:"Spring",4:"Summer"}
    fmt = "{:s} {:d}-{:d}"
    return fmt.format(termNames[term],(year-1)%100,year%100)

#Course Average with weights
def courseAverage(key,DB):
	top=0
	bottom=0
	L = DB[key]
	for X in L:
		top += X[0]*X[1]
		bottom += X[1]
	return top/bottom
#Instructor Average with weights
def instructorAverage(key,DB):
	top=0
	bottom=0
	L = DB[key]
	for X in L:
		top += X[2]*X[1]
		bottom += X[1]
	return top/bottom

def makePlot(course, minYear, maxYear, DB, targetFile):
    if course==0:
        title="Ratings for all Courses from {:d} to {:d}"
        title=title.format(minYear,maxYear)
    else:
        #Generate the title of the table
        title="Ratings for CS-{:d} from {:d} to {:d}"
        title=title.format(course,minYear,maxYear)
    #Lists for Plot Generation
    X = []
    Ycourse = []
    Yinstructor = []
    #Loop Over the DB and get what we need
    for year in range(minYear,maxYear+1):
        for term in range(1,5):
            key=(year,term,course)
            if key in DB.keys():
                courseAvgRate = courseAverage(key,DB)
                instructorAvgRate = instructorAverage(key,DB)
                X.append(termName(year,term))
                Ycourse.append(courseAvgRate)
                Yinstructor.append(instructorAvgRate)
    #Enumerate the Labels into numbers
    x_pos = [i for i, _ in enumerate(X)]
    #How wide should the bars be
    w=0.3
    #Start up the plot
    plt.title(title)
    plt.xlabel("Term")
    plt.ylabel("Rating")
    #Add Data
    courseBars = plt.bar(x_pos, Ycourse,width=w, color='blue')
    instructorBars = plt.bar([a+w for a in x_pos], Yinstructor,width=w, color='red')
    #Legend
    plt.legend( (courseBars[0], instructorBars[0]), ('Course', 'Instructor'), loc='lower right' )
    #Rotate the X axis for long names
    plt.xticks(rotation = 85)
    plt.xticks(x_pos, X)
    #Focus on the interesting part
    axes = plt.gca()
    axes.set_ylim([0,5])
    #Save as file (or show for debugging)
    #plt.show()
    plt.tight_layout()
    #Make File Name
    plt.savefig(targetFile)
    plt.clf()
    
                
#Generate Plots
def main(argv):
    if len(argv)!=4:
        print("Usage: python change.py [course] [rating file] [target file]")
        print("Use 0 to mean all courses.")
        return
    #Parse Command Line Args
    course = int(argv[1])
    filename = argv[2]
    target = argv[3]
    #Make the Database
    DB = parseDatabase(filename)
    #Which Course are in the file
    courseList = getCourses(DB)
    #Which Years are Covered
    yearList = getYears(DB)
    #Generate One Plot
    makePlot(course, min(yearList), max(yearList), DB, target)
    print("Completed")

#Run if not imported as library
if __name__=="__main__":
    main(sys.argv)
