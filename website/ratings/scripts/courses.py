#Mark Boady

#Make a JSON with just the file list

#Libraries
import sys
import json
import csv

#Just get the course names
def parse(filename):
    f = open(filename,"r")
    reader = csv.reader(f)
    next(reader)#Throw away header
    #Make a set for names
    courseNames = set([])
    #Read the File
    for line in reader:
        course = line[0]
        courseNames.add(course)
    f.close()
    L = list(courseNames)
    L.sort()
    return L

#Save the results
def save(DB, target):
    f = open(target,"w")
    f.write(json.dumps(DB))
    f.close()

#Main Program
def main(argv):
    if len(sys.argv)!=3:
        print("Usage: python courses.py [input csv] [output json]")
        return
    inputFile = sys.argv[1]
    outputFile = sys.argv[2]
    DB = parse(inputFile)
    save(DB,outputFile)

if __name__=="__main__":
    main(sys.argv)
