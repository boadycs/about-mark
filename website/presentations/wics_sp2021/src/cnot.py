#Mark Boady - 2021
#Intro to Quantum Computers

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer


#CNOT Example
qc = QuantumCircuit(2,2)
#Add a gate
qc.cnot(0,1)

#Print as text
print(qc.draw(output="text"))
#Latex for Slides
print(qc.draw(output="latex_source"))
