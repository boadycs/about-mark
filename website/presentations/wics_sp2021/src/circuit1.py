#Mark Boady - 2021
#Intro to Quantum Computers

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer


#I want a circuit with
#3 Qubits and 3 classic bits
qc = QuantumCircuit(3,3)

#Add a gate
qc.x(0)

#We need to measure to see the results
#Barrier is just for visual
qc.barrier(range(0,3))
qc.measure(0,0)
qc.measure(1,1)
qc.measure(2,2)

#Print as text
print(qc.draw(output="text"))
#Latex for Slides
print(qc.draw(output="latex_source"))
#Matplot to make an image
qc.draw(output="mpl",filename="cicuit.png")

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)
