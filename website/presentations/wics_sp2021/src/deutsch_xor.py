#Mark Boady
#Implementation of Deutsch-Jozsa Algorithm
from qiskit import *
from qiskit import BasicAer

#3-qubits and 2 classic
qc = QuantumCircuit(3,2)

#Build a Circuit
#Step 1: Apply H to the input bits
qc.h(0)
qc.h(1)
#Step 2: Apply X to the result bit
#then apply H
qc.x(2)
qc.h(2)

#Step 3: Implement Boolean Function
qc.barrier()
qc.cnot(0,1)
qc.cnot(1,2)
qc.cnot(0,1)
qc.barrier()

#Step 4: Apply H to all the inputs
qc.h(0)
qc.h(1)
#Step 5: Measure inputs
qc.measure(0,0)
qc.measure(1,1)

print(qc.draw(output="text"))
X=qc.draw(output="latex_source")
print(X)

#Run an experiment!
backend_sim = BasicAer.get_backend('qasm_simulator')
job_sim = execute(qc,backend_sim,shots=2048)
result_sim = job_sim.result()
counts = result_sim.get_counts()
print(counts)
