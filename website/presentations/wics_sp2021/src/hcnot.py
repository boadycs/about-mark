#Mark Boady - 2021
#Intro to Quantum Computers

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer


#CNOT Example
qc = QuantumCircuit(2,2)
#Add a gate
qc.h(0)#Superposition q0
qc.x(1)#Start q1 as 1
qc.cnot(0,1)
#Measure Results
qc.measure(0,0)
qc.measure(1,1)

#Print as text
print(qc.draw(output="text"))
#Latex for Slides
print(qc.draw(output="latex_source"))


#Run an experiment!
backend_sim = BasicAer.get_backend('qasm_simulator')
job_sim = execute(qc,backend_sim,shots=2048)
result_sim = job_sim.result()
counts = result_sim.get_counts()
print(counts)
